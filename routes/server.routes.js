const express = require("express");
const router = express.Router();
const officers = require("../controllers/officer.controller");
const leaves = require("../controllers/leave.controller");
const leaveList = require("../controllers/leaveList.controller");

router.get("/officers", officers.findAll);

router.get("/active", officers.findActive);

router.get("/resigned", officers.findResigned);

router.get("/officers-pagination", officers.findAllPagination);

router.get("/officer-search", officers.findOne);

router.get("/search", officers.search);

router.post("/officer", officers.add);

router.put("/officer", officers.edit);

router.delete("/officer", officers.delete);

router.get("/leaves", leaves.findAll);

router.get("/active-leave", leaves.findActive);

router.post("/leave", leaves.add);

router.put("/leave", leaves.edit);

router.delete("/leave", leaves.delete);

router.put("/add-leaves", leaves.addLeaveList);

router.get("/leave-lists", leaveList.findAll);

router.post("/leave-list", leaveList.add);

router.put("/leave-list", leaveList.edit);

router.delete("/leave-list", leaveList.delete);

module.exports = router;
