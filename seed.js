const faker = require("faker");
const dbConfig = require("./config/db.config");
const mongoose = require("mongoose");
const OfficerModel = require("./models/officer.model");

async function seedDB() {
  try {
    await mongoose
      .connect(dbConfig.cloudURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      .then(
        () => {
          console.log("[Success] : Connected to the database!");
        },
        (error) => {
          console.log("[Failed] : Can't connect to the database!", error);
          process.exit();
        }
      );

    mongoose.connection.db.dropCollection("officers");

    let officersData = [];
    for (let i = 0; i < 100; i++) {
      const idCard = faker.time.recent();
      const firstName = faker.name.firstName();
      const lastName = faker.name.lastName();
      const position = faker.name.jobType();
      let officer = { idCard, firstName, lastName, position };
      officersData.push(officer);
    }
    OfficerModel.insertMany(officersData)
      .then(() => console.log("[Success] : Database seeded!"))
      .catch((e) => {
        console.log(err);
      });
  } catch (err) {
    console.log(err);
  }
}

seedDB();
