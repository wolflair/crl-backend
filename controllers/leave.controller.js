const LeaveModel = require("../models/leave.model");

exports.findAll = (req, res) => {
  LeaveModel.find()
    .then((leave) => res.json(leave))
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.findActive = (req, res) => {
  LeaveModel.find({
    status: 'active'
  })
    .then((officers) => res.json(officers))
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.add = (req, res) => {
  const payload = req.body;
  const leave = new LeaveModel(payload);
  leave
    .save()
    .then(res.status(201).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.edit = (req, res) => {
  const payload = req.body;
  LeaveModel.findOneAndUpdate({idCard: payload.idCard}, { $set: payload })
    .then(res.status(200).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.delete = (req, res) => {
  const id = req.query.id;
  LeaveModel.findByIdAndDelete(id)
    .then(res.status(200).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.addLeaveList = (req, res) => {
  const payload = req.body;
  LeaveModel.findOneAndUpdate(
    { idCard: payload.idCard },
    {
      $inc: {
        leave: payload.leave,
        personalLeave: payload.personalLeave,
        sickLeave: payload.sickLeave,
        remainingLeave: -(
          payload.leave +
          payload.personalLeave +
          payload.sickLeave
        ),
      },
    }
  )
    .then(res.status(200).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};
