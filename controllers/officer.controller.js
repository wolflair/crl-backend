const OfficerModel = require("../models/officer.model");

const getPagination = (page, size) => {
  const limit = size ? +size : 10;
  const offset = page > 0 ? (page - 1) * limit : 0;

  return { page, limit, offset };
};

exports.findAll = (req, res) => {
  OfficerModel.find()
    .then((officers) => res.json(officers))
};

exports.findOne = (req, res) => {
  const query = req.query;
  OfficerModel.findOne(query)
    .then((officer) => res.json(officer))
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.findActive = (req, res) => {
  OfficerModel.find({
    status: 'active'
  })
    .then((officers) => res.json(officers))
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.findResigned = (req, res) => {
  OfficerModel.find({
    status: 'resigned'
  })
    .then((officers) => res.json(officers))
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.search = (req, res) => {
  const query = req.query.keyword;
  OfficerModel.find({
    $or: [
      { firstName: { $regex: query } },
      { lastName: { $regex: query } },
      { position: { $regex: query } },
      { department: { $regex: query } },
    ],
  })
    .then((officer) => res.json(officer))
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.findAllPagination = (req, res) => {
  const { page, size } = req.query;
  const { limit, offset } = getPagination(page, size);

  OfficerModel.paginate({}, { limit, offset })
    .then((officers) => res.json(officers))
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.add = (req, res) => {
  const payload = req.body;
  const officer = new OfficerModel(payload);
  officer
    .save()
    .then(res.status(201).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.edit = (req, res) => {
  const payload = req.body;
  OfficerModel.findByIdAndUpdate(payload._id, { $set: payload })
    .then(res.status(200).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.delete = (req, res) => {
  const id = req.query.id;
  OfficerModel.findByIdAndDelete(id)
    .then(res.status(200).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};
