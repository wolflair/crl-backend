const LeaveListModel = require("../models/leaveList.model");

exports.findAll = (req, res) => {
  LeaveListModel.find()
    .then((leaveList) => res.json(leaveList))
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.add = (req, res) => {
  const payload = req.body;
  const leaveList = new LeaveListModel(payload);
  leaveList
    .save()
    .then(res.status(201).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.edit = (req, res) => {
  const payload = req.body;
  LeaveListModel.findByIdAndUpdate(payload._id, { $set: payload })
    .then(res.status(200).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};

exports.delete = (req, res) => {
  const id = req.query.id;
  LeaveListModel.findByIdAndDelete(id)
    .then(res.status(200).end())
    .catch((e) => {
      res.status(500).send({ message: e.message });
    });
};
