const app = require("../server");
const mongoose = require("mongoose");
const request = require("supertest");

const { MongoMemoryServer } = require("mongodb-memory-server");

jest.setTimeout(60000);

beforeAll(async () => {
  const mongod = await MongoMemoryServer.create();
  await mongoose.createConnection(mongod.getUri(), {
    useNewUrlParser: true,
    autoReconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
  });
});

afterEach(async () => {
  const collections = mongoose.connection.collections;

  for (const key in collections) {
    const collection = collections[key];
    await collection.deleteMany();
  }
});

afterAll(async () => {
  const mongod = await MongoMemoryServer.create();
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongod.stop();
});

describe("Test Officers API", () => {
  describe("List Officers", () => {
    it("returns a list of officers", async () => {
      const response = await request(app).get("/officers");
      expect(response.statusCode).toBe(200);
    });

    it("returns a list of active officers", async () => {
      const response = await request(app).get("/active");
      expect(response.statusCode).toBe(200);
    });

    it("returns a list of resigned officers", async () => {
      const response = await request(app).get("/resigned");
      expect(response.statusCode).toBe(200);
    });

    it("search officers with idCard", async () => {
      const response = await request(app).get(
        "/officer-search?idCard=01234567891012"
      );
      expect(response.statusCode).toBe(200);
    });

    it("search officers with keyword", async () => {
      const response = await request(app).get("/search?keyword=Dev");
      expect(response.statusCode).toBe(200);
    });
  });

  describe("Create Officer", () => {
    it("create new officer", async () => {
      const response = await request(app).post("/officer").send({
        idCard: "01234567891011",
        firstName: "Pawaret",
        lastName: "Muengkaew",
        position: "Developer",
        status: "active",
      });
      expect(response.statusCode).toBe(201);
    });
  });

  describe("Edit Officer", () => {
    it("edit officer", async () => {
      const response = await request(app).put("/officer").send({
        _id: "61445c058e15f83148fd2394",
        idCard: "01234567891011",
        firstName: "Apiwit",
        lastName: "Thammachai",
        position: "Maid",
      });
      expect(response.statusCode).toBe(200);
    });

    it("edit officer status to resigned", async () => {
      const response = await request(app).put("/officer").send({
        _id: "61445c058e15f83148fd2394",
        status: "resigned",
      });
      expect(response.statusCode).toBe(200);
    });
  });

  describe("Delete Officer", () => {
    it("delete officer", async () => {
      const response = await request(app).delete("/officer");
      expect(response.statusCode).toBe(200);
    });
  });
});

describe("Test Leave API", () => {
  describe("List Leaves", () => {
    it("returns a list of leaves", async () => {
      const response = await request(app).get("/leaves");
      expect(response.statusCode).toBe(200);
    });

    it("returns a list of active officers leave", async () => {
      const response = await request(app).get("/active-leave");
      expect(response.statusCode).toBe(200);
    });
  });

  describe("Create Officer Leave", () => {
    it("create new officer leave", async () => {
      const response = await request(app).post("/leave").send({
        idCard: "01234567891011",
        leave: 12,
        personalLeave: 12,
        sickLeave: 10,
        status: 'active'
      });
      expect(response.statusCode).toBe(201);
    });
  });

  describe("Edit Officer Leave", () => {
    it("edit officer leave", async () => {
      const response = await request(app).put("/leave").send({
        idCard: "01234567891011",
        leave: 13,
        personalLeave: 2,
        sickLeave: 14,
        status: 'resigned'
      });
      expect(response.statusCode).toBe(200);
    });
  });

  describe("Delete Officer Leave", () => {
    it("delete officer leave", async () => {
      const response = await request(app).delete("/leave");
      expect(response.statusCode).toBe(200);
    });
  });
});

describe("Test Leave List API", () => {
  describe("List of Leave Lists", () => {
    it("returns a list of leave list", async () => {
      const response = await request(app).get("/leave-lists");
      expect(response.statusCode).toBe(200);
    });
  });

  describe("Create Leave List", () => {
    it("create new leave list", async () => {
      const response = await request(app).post("/leave-list").send({
        idCard: "01234567891011",
        description: "test",
        leave: 12,
        personalLeave: 12,
        sickLeave: 10,
      });
      expect(response.statusCode).toBe(201);
    });
  });

  describe("Edit Leave List", () => {
    it("edit leave list", async () => {
      const response = await request(app).put("/leave-list").send({
        idCard: "01234567891011",
        description: "edit test",
        leave: 24,
        personalLeave: 2,
        sickLeave: 11,
      });
      expect(response.statusCode).toBe(200);
    });
  });

  describe("Delete Leave List", () => {
    it("delete leave list", async () => {
      const response = await request(app).delete("/leave-list");
      expect(response.statusCode).toBe(200);
    });
  });
});


