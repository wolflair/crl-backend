require("dotenv").config()

module.exports = {
  // MongoDB Cloud
  cloudURL:
    process.env.MONGO_CLOUD_URL,
  // MongoDB Docker
  dockerURL: process.env.MONGO_DOCKER_URL,
  // MongoDB Local
  localURL: process.env.MONGO_LOCAL_URL
};
