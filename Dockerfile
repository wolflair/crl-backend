FROM node:14

ENV APP_DIR /usr/src/app/server
WORKDIR ${APP_DIR}

COPY package*.json ./

RUN npm install

COPY . .

CMD ["npm", "start"]