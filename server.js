const express = require("express");
const app = express();
const dbConfig = require("./config/db.config");
const mongoose = require("mongoose");
const router = require("./routes/server.routes");

mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false)

// Connect MongoDB
mongoose
  .connect(dbConfig.cloudURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(
    () => {
      console.log("[Success] : Connected to the database!");
    },
    (error) => {
      console.log("[Failed] : Can't connect to the database!", error);
      process.exit();
    }
  );

app.use(express.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "DELETE, PUT");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  if ("OPTIONS" == req.method) {
    res.sendStatus(200);
  } else {
    next();
  }
});

app.get("/", (req, res) => {
  res.send("### CRL Server Started! ###");
});

app.use("/", router);

const PORT = process.env.PORT || 9000;
if (process.env.NODE_ENV !== 'test') {
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
}

module.exports = app;