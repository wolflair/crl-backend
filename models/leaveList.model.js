const mongoose = require('mongoose')
const Schema = mongoose.Schema

const leaveListSchema = new Schema(
  {
    idCard: String,
    description: String,
    leave: Number,
    personalLeave: Number,
    sickLeave: Number,
  },
  {
    timestamps: true,
    versionKey: false,
  },
)

const LeaveListModel = mongoose.model('LeaveList', leaveListSchema)

module.exports = LeaveListModel
