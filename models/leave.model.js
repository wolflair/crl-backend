const mongoose = require('mongoose')
const Schema = mongoose.Schema

const leaveSchema = new Schema(
  {
    idCard: String,
    leave: Number,
    personalLeave: Number,
    sickLeave: Number,
    remainingLeave: Number,
    status: String,
  },
  {
    timestamps: true,
    versionKey: false,
  },
)

const LeaveModel = mongoose.model('Leave', leaveSchema)

module.exports = LeaveModel
