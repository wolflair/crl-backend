const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const Schema = mongoose.Schema

const officerSchema = new Schema(
  {
    idCard: String,
    firstName: String,
    lastName: String,
    department: String,
    position: String,
    workingPeroid: Number,
    peroidUnit: String,
    status: String,
  },
  {
    timestamps: true,
    versionKey: false,
  },
)

officerSchema.plugin(mongoosePaginate)

const OfficerModel = mongoose.model('Officer', officerSchema)

module.exports = OfficerModel
